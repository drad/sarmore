# README

sarmore is the SAR orchistrator, a poor man's metric monitor. sarmore is a wrapper to call sar (as it is lightweight and generally available across distros). sarmore has been created to provide a light metrics monitor for use in stress/load testing a server.

### Requirements

- sar should be available on most servers, if not you may need to install `sysstat`

### How It Works

The general flow for using sarmore is as follows:
- configure sarmore (by setting the RUN_LENGTH, RUN_INTERVAL, LOG_DIR envvars)
  - default should be fine but you can adjust if needed
- start your load test (and let it run!)
- start sarmore (it will stop automatically after the specified RUN_LENGTH amount of time has passed)
- after sarmore has stopped you can stop your load test
  - you can check if sarmore is done by tailing any one of the output files, upon completion the 'Average: ' row will be displayed
- view the results produced by sarmore

## Best Practices

- sarmore runtime 600 (10m)
- run your test for 11m
- start sarmore 30s after test was started

## Example Run

- using defaults: `sarmore`
- set RUN_LENGTH to 1200s: `RUN_LENGTH=1200 sarmore`
- change LOG_DIR: `LOG_DIR=/tmp sarmore`
